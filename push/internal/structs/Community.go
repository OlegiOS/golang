package structs


type Community struct {
	Clans []Clan `json:"community, []"`
}

type Clan struct {
	ClanID           string       `json:"clanid, string"`
	ClanName         string       `json:"name, string"`
	ClanTag          string       `json:"tag, string"`
	RecruitingStatus string       `json:"statusClan, string"`
	Description      string       `json:"desc, string"`
	CreatedDate      string       `json:"cdate, string"`
	Slogan           string       `json:"slogan, string"`
	ClanMembers      []ClanMember `json:"members, []"`
}


type ClanMember struct {
	Nick      string  `json:"nick,string"`
	EntryDate int64   `json:"date,string"`
	ClanRole  int64   `json:"role,string"`
	UID       int64   `json:"uid,string"`
	ArcRatio  float64 `json:"drEra5Arc,string"`
	HistRatio float64 `json:"drEra5Hist,string"`
	SimRatio  float64 `json:"drEra5Sim,string"`
}
