package main

import (
	"encoding/json"
	"fmt"
	"github.com/appleboy/go-fcm"
	"io/ioutil"
	"log"
	"os"
	"github.com/davecgh/go-spew/spew"
)

type Community struct {
	Clans []Clan `json:"community"`
}

type Clan struct {
	ClanID           string       `json:"clanid"`
	ClanName         string       `json:"name"`
	ClanTag          string       `json:"tag"`
	RecruitingStatus string       `json:"statusClan"`
	Description      string       `json:"desc"`
	CreatedDate      string       `json:"cdate"`
	Slogan           string       `json:"slogan"`
	ClanMembers      []ClanMember `json:"members"`
}


type ClanMember struct {
	Nick      string  `json:"nick"`
	EntryDate int64   `json:"date,string,omitempty"`
	ClanRole  int64   `json:"role,string,omitempty"`
	UID       int64   `json:"uid,string,omitempty"`
	ArcRatio  float64 `json:"drEra5Arc,string,omitempty"`
	HistRatio float64 `json:"drEra5Hist,string,omitempty"`
	SimRatio  float64 `json:"drEra5Sim,string,omitempty"`
}

const (
	serverKey = "AAAA70vJieU:APA91bHbFhmbDFBKLKGrPaMmChexPnHLaVptG0Kk9FN8IfDH5sQX892rpVeRaYDviM5q3IUpuVrRgvcuAk0D7VRAgJA_vatl2kyQE3OZXNOSXSiaYPQ90cIFGfflfXHZDEsiO85l6W19"
)


func getClanById(clanID string) (Clan, error) {
	data, err := ioutil.ReadFile("./templates/clanInfo.json")
	if err != nil {
		spew.Dump(err)
	}

	var community Community

	err = json.Unmarshal(data, &community)
	if err != nil {
		spew.Dump(err)
	}

	spew.Dump(community.Clans)

	var c Clan
	if err != nil {
		for i := range community.Clans {
			clan := community.Clans[i]
			if clan.ClanID == clanID {
				c = clan
				break
			}
		}
	}

	return c, err
}

func sendFCMToClient(text string, pushToken string) {

	n := fcm.Notification{Body:"body", Title:"title"}

	msg := &fcm.Message{
		To:pushToken,
		Notification:&n,
		Data: map[string]interface{}{
			"foo": "bar",
		},
	}

	client , err := fcm.NewClient(serverKey)
	if err != nil {
		log.Fatalln(err)
	}

	response, err := client.Send(msg)
	if err != nil {
		log.Fatalln(err)
	}

	log.Printf("%#v\n", response)
}

func main() {
	clanID := "1029682"

	clan, err := getClanById(clanID)
	if err != nil {
		spew.Dump(err)
		os.Exit(1)
	}

	fmt.Println(clan)

	token := "f5tbvVklOBI:APA91bGDiO9s9FtsDEEtXpsdG8lHNroUGkHvHZJhQl_yoxidl65QMON4J1NNPKTWWtyW440axo5vfF_6tz_wfDWjLLyyEOhbckTRtVekT_5Mfc1HE6HLTbSgCFsqgZHWGkBgyBxb33fF"
	sendFCMToClient("hello", token)
}
